package com.peopleflow;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTest {

    public static void main(String[] args) throws InterruptedException {
        try (ServerSocket server = new ServerSocket(3345)) {
            Socket client = server.accept();

            DataOutputStream out = new DataOutputStream(client.getOutputStream());

            DataInputStream in = new DataInputStream(client.getInputStream());

            while (!client.isClosed()) {
                System.out.println("Server reading from channel");

                String entry = in.readUTF();

                System.out.println("READ from client message - " + Math.pow(Integer.parseInt(entry),2));

                if (entry.equalsIgnoreCase("quit")) {
                    out.writeUTF("Server reply - " + entry + " - OK");
                    out.flush();
                    Thread.sleep(3000);
                    break;
                }

                out.writeUTF("Server reply - " + entry + " - OK");

                out.flush();
            }

            System.out.println("Client disconnected");

            in.close();
            out.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
